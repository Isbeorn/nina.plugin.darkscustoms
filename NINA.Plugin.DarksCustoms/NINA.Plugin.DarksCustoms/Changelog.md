﻿# Changelog

## Version 1.1.0.0

- Added the filter offset calculator as a tool into the imaging area
- Improved progress report while running by showing the number of iterations and number of filters per iteration

## Version 1.0.0.0

- Initial release